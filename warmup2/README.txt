Documentation for Warmup Assignment 1

=====================================



+-------+

| BUILD |

+-------+


Comments:  make clean
	  
	   make




+---------+

| GRADING |

+---------+



(A) Doubly-linked Circular List : 40 out of 40 pts



(B.1) Sort from (file) : 30 out of 30 pts

(B.2) Sort from (stdin) : 30 out of 30 pts




Missing required section(s) in README file : No Comments


Submitted Binary file : No

Cannot compile : No Comments


Compiler warnings : No Comments


"make clean" : No Comments


Segmentation faults : No Comments


Separate compilation : No Comments


Malformed input : No Comments


Too slow : No Comments


Bad commandline : No Comments


Bad behavior for random input :No Comments


Did not use My402List and My402ListElem to implement "sort" in (B) : No Comments




+------+

| BUGS |

+------+



Comments: No Comments




+-------+

| OTHER |

+-------+




Comments on design decisions:     While reading input from the stdin(keyboard), please give a new line character followed by ctrl+D for the transaction table to print.

Comments on deviation from spec:  No Comments

