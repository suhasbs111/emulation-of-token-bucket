#include<stdio.h>
#include<pthread.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<sys/time.h>
#include<unistd.h>
#include<getopt.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<signal.h>
#include"cs402.h"
#include"my402list.h"
#include"globals.h"


double getseconds(struct timeval i)
{
	double t= (((i.tv_sec)*1000000) + i.tv_usec);
	return t;
}

char gszProgName[MAXPATHLENGTH];


//Function to list the usage of the command to the user
void Usage()
{
    fprintf(stderr,"usage: %s %s\n","warmup2" , " [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]");
    exit(-1);
}

//Function to set the program name
void SetProgramName(char *s)
{
    char *c_ptr=strrchr(s, DIR_SEP);
    if (c_ptr == NULL) {
        strcpy(gszProgName, s);
    } else {
        strcpy(gszProgName, ++c_ptr);
    }
}

void printtime(struct timeval t)
{
double time;
emulationEndTime=time=(double)(getseconds(t)-getseconds(globaltim));
fprintf(stdout,"%012.3lfms: ",time/1000);
}

void ProcessFile()
{	struct stat buf;
	if(stat(fileName,&buf)!=0)
       {
            fprintf(stderr,"file path %s is not valid - %s\n",fileName,strerror(errno));
            Usage();
            exit(-1);
       }
       int i=S_ISREG(buf.st_mode);
       if(i==0)
       {
           fprintf(stderr,"%s is a directory or other type of file\n",fileName);
           Usage();
           exit(-1);
       }
       if(buf.st_size==0)
       {
           fprintf(stderr,"%s is an empty file\n",fileName);
          Usage();
          exit(-1);
       }
       if((fp=fopen(fileName,"r"))==NULL)
       {
           fprintf(stderr,"Malformed input file, %s - %s\n",fileName,strerror(errno));
           Usage();
           exit(-1);    
       }
}

void ReadFirstLine()
{
	memset(line,0,sizeof(line));
	if(fgets(line,sizeof(line),fp)!=NULL)
      	{
		if(atoi(line)<=0)
		{
			fprintf(stderr,"Malformed file - Invalid number of packets\n");
			exit(-1);
		}
		numberOfPackets=atoi(line);
	}
}

void ReadTransaction()
{
	int fieldnumber=0;
	int i=strlen(line);
	char *token=NULL;
	if(line[0]==' ' || line[0]=='\t')
	{
		fprintf(stderr, "Malformed input at line%d\n",linenumber+1);
        	exit(-1);
	}
	token=strtok(line,"\t ");
	for(i=0;i<strlen(token);i++)
        {
		if(((token[i]-'0')< 0) || ((token[i]-'0')> 9))
		{
			fprintf(stderr,"Malformed interarrival time at line %d, contains invalid characters\n",linenumber+1);
                 	exit(-1);
		}
	
	}
	sleeparrivaltime=(double)atoi(token)*1000;
	fieldnumber++;
	while(token!=NULL)
	{
      		token=strtok(NULL,"\t ");
		if(fieldnumber==1)
		{
			for(i=0;i<strlen(token);i++)
	        	{
				if(((token[i]-'0')< 0) || ((token[i]-'0')> 9))
				{
					fprintf(stderr,"Malformed token field at line %d, contains invalid characters\n",linenumber+1);
	        	         	exit(-1);
				}
			}
			P=atoi(token);
		}
		else if(fieldnumber==2)
		{
			for(i=0;i<strlen(token);i++)
	        	{
				if((((token[i]-'0')< 0) || ((token[i]-'0')> 9)) && ((token[i])!='\n') && ((token[i])!='\r'))
				{
					fprintf(stderr,"Malformed service time field at line %d, contains invalid characters\n",linenumber+1);
	        	         	exit(-1);
				}
			}
			if(token[strlen(token)-1]=='\n' || token[strlen(token)-1]=='\r')
			{
				token[strlen(token)-1]='\0';
			}
			if(token[strlen(token)-1]=='\n' || token[strlen(token)-1]=='\r')
			{
				token[strlen(token)-1]='\0';
			}
			sleepservicetime=(double)atoi(token)*1000;
		}
		fieldnumber++;
	}
	
	if(fieldnumber!=4)
   	{
      		fprintf(stderr,"Malformed input at line %d - might have more than 3 fields\n",linenumber+1);
      		exit(-1);
   	}

}

void ReadFile()
{
	if(fgets(line,sizeof(line),fp)!=NULL)
      	{
		if((strrchr(line,'\n')==NULL))
		{
			fprintf(stderr, "Record details are invalid, does not contain new line on line %d\n",count+1);
             		exit(-1);
		}
		ReadTransaction();
		linenumber++;
			
	}
      		
	
	/*if(linenumber==0)
   	{
        	fprintf(stderr,"No Transactions present in the specified file\n");
        	exit(-1);
   	}*/

}

void printEmulationParameters()
{
	fprintf(stdout,"Emulation Parameters\n");
	if(files)
	{
		fprintf(stdout,"\tnumber to arrive = %d\n",numberOfPackets);
		fprintf(stdout,"\tr = %lf\n",r);
		fprintf(stdout,"\tB = %d\n",B);
		fprintf(stdout,"\ttsfile = %s\n",fileName);
	}
	else
	{
		fprintf(stdout,"\tnumber to arrive = %d\n",numberOfPackets);
		fprintf(stdout,"\tlambda = %lf\n",lambda);
		fprintf(stdout,"\tmu = %lf\n",mu);
		fprintf(stdout,"\tr = %lf\n",r);
		fprintf(stdout,"\tB = %d\n",B);		
		fprintf(stdout,"\tP = %ld\n",P);
		
	}
}

void *arrivalthreadProcess(void *args)
{
	packet *pa=NULL;
	struct timeval tim;
	double arrtime;
	pthread_mutex_lock(&m);
	if(files)
	{
		ProcessFile();
		ReadFirstLine();
	}	
	printEmulationParameters();
       	fprintf(stdout,"00000000.000ms: emulation begins\n");
	pthread_mutex_unlock(&m);
	sleeparrivaltime=(1/lambda)*(double)1000000;
	double qonearrivalTime,qonedeparttime,prevpackettime=(double)getseconds(globaltim);
	if((1/lambda)>10)
		sleeparrivaltime=10000000;
	double actualsleeptime=sleeparrivaltime;	
	while(count<numberOfPackets)
	{	
		if(!files)
		{	gettimeofday(&tim,NULL);
			sleeparrivaltime=actualsleeptime-((double)getseconds(tim)-prevpackettime);
			sleepservicetime=(1/mu)*(double)1000000;
				if((1/mu)>10)
			sleepservicetime=10000000;
		}
		else
		{	
			ReadFile();
			gettimeofday(&tim,NULL);
			sleeparrivaltime=sleeparrivaltime-((double)getseconds(tim)-prevpackettime);;
		}
		if(sleeparrivaltime>0)
		{
			usleep((useconds_t)sleeparrivaltime);
		}
		//int oldstate;
		//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,&oldstate);
		pthread_testcancel();
		pthread_mutex_lock(&m);
		pa=(packet *)malloc(sizeof(packet));
                pa->packetNum=++count;
		pa->tokensrequired=P;
		pa->servicetimeofpacket=sleepservicetime;
		
 		gettimeofday(&tim,NULL);
		arrtime=getseconds(tim);
		pa->arrivalTime=(double)arrtime-(double)getseconds(globaltim);
		if(pa->tokensrequired>B)
  		{
			printtime(tim);
			if(pa->tokensrequired==1)
			{
			fprintf(stdout,"p%d arrives, needs %ld token,interarrival time = %.3lfms dropped\n",pa->packetNum,pa->tokensrequired,(arrtime-prevpackettime)/(double)1000);
			}
			else
			{
			fprintf(stdout,"p%d arrives, needs %ld tokens,interarrival time = %.3lfms dropped\n",pa->packetNum,pa->tokensrequired,(arrtime-prevpackettime)/(double)1000);
			}
			totalInterArrivalTime=totalInterArrivalTime+(arrtime-prevpackettime);
			prevpackettime=arrtime;
			numofPacketsDropped++;
			//gettimeofday(&tim,NULL);
			free(pa);
			pthread_mutex_unlock(&m);
			pthread_testcancel();
			continue;
		}
		printtime(tim);
		if(pa->tokensrequired==1)
		{
		fprintf(stdout,"p%d arrives, needs %ld token, interarrival time = %.3lfms\n",pa->packetNum,pa->tokensrequired,(arrtime-prevpackettime)/(double)1000);
		}
		else
		{
		fprintf(stdout,"p%d arrives, needs %ld tokens, interarrival time = %.3lfms\n",pa->packetNum,pa->tokensrequired,(arrtime-prevpackettime)/(double)1000);
		}
		totalInterArrivalTime=totalInterArrivalTime+(arrtime-prevpackettime);
		prevpackettime=arrtime;
		pthread_testcancel();
		gettimeofday(&tim,NULL);
		pa->q1ArrivalTime=qonearrivalTime=(double)(getseconds(tim)-getseconds(globaltim)); 
		printtime(tim);
		My402ListPrepend(&queue1,(void *)pa);
		fprintf(stdout,"p%d enters Q1\n",pa->packetNum);
		My402ListElem *elem=My402ListLast(&queue1);
		packetsInQone++;
		pthread_testcancel();
		void *data = (void *)elem->obj;
		packet *ps=(packet *)data;
		if(requiredtoken>=ps->tokensrequired)
		{
			
				My402ListUnlink(&queue1,elem);
				requiredtoken=requiredtoken-ps->tokensrequired;
				gettimeofday(&tim,NULL);
				ps->q1EndTime=qonedeparttime=(double)(getseconds(tim)-getseconds(globaltim));
				printtime(tim);
				if(requiredtoken==1 || requiredtoken==0)
				{
				fprintf(stdout,"p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %ld token\n",pa->packetNum,(qonedeparttime-qonearrivalTime)/1000,requiredtoken);
				}
				else
				{
				fprintf(stdout,"p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %ld tokens\n",pa->packetNum,(qonedeparttime-qonearrivalTime)/1000,requiredtoken);
				}
				//totalTimeOfAllPacketsInQone=totalTimeOfAllPacketsInQone+(qonedeparttime-qonearrivalTime);
				//pthread_testcancel();
				gettimeofday(&tim,NULL);				
				ps->q2ArrivalTime=(double)(getseconds(tim)-getseconds(globaltim));
				PacketsInQtwo++;
				My402ListPrepend(&queue2,(void *)data);
				printtime(tim);
				fprintf(stdout,"p%d enters Q2\n",pa->packetNum);
				if(++service==1)
				{
					pthread_cond_signal(&cv);
				}
				pthread_testcancel();
			
		}
		
              pthread_mutex_unlock(&m);
                	
	}
	pthread_mutex_lock(&m);
	pthread_cond_signal(&cv);
	pthread_mutex_unlock(&m);
	arrivalexit=TRUE;
	pthread_exit(0);

}

void *tokenthreadProcess(void *args)
{
	struct timeval tim;	
	sleeptokentime=(1/r)*(double)1000000;
	double qonedeparttime,arrtime;
	if(!files)
	{
		if((1/r)>10)
			sleeptokentime=10000000;
	}	
	double delaytime=(double)getseconds(globaltim);
	double actualsleeptime=sleeptokentime;
	while(1 && !shutdown)
	{
		gettimeofday(&tim,NULL);
		sleeptokentime=actualsleeptime-((double)getseconds(tim)-delaytime);
		if(sleeptokentime>0)
		{
			usleep((useconds_t)sleeptokentime);
		}
				
		pthread_mutex_lock(&m);
		if(My402ListEmpty(&queue1) && (count == numberOfPackets))
		{
			break;
		}
		pthread_testcancel();
                totaltokens++;
		gettimeofday(&tim,NULL);
		arrtime=getseconds(tim);
		delaytime= arrtime;
		if(requiredtoken!=B)
		{
			requiredtoken++;
			printtime(tim);
			if(requiredtoken==1)
			{
			fprintf(stdout,"token t%ld arrives, token bucket now has %ld token\n",totaltokens,requiredtoken);
			}
			else
			{
			fprintf(stdout,"token t%ld arrives, token bucket now has %ld tokens\n",totaltokens,requiredtoken);
			}			
			pthread_testcancel();
		}
		else
		{
				numOfDroppedTokens++;
				printtime(tim);
				fprintf(stdout,"token t%ld arrives, dropped\n",totaltokens);
				pthread_testcancel();
		}
		
		if(!My402ListEmpty(&queue1) && count<=numberOfPackets)
		{
			My402ListElem *elem=My402ListLast(&queue1);
			void *data = (void *)elem->obj;
			packet *ps=(packet *)data;
			if(ps->tokensrequired>B && requiredtoken==B)
			{
				numOfDroppedTokens++;
				printtime(tim);
				fprintf(stdout,"token t%ld arrives, dropped\n",totaltokens);
				pthread_testcancel();
			}
			
			if(requiredtoken>=ps->tokensrequired)
			{
				My402ListUnlink(&queue1,elem);
				requiredtoken=requiredtoken-ps->tokensrequired;
				gettimeofday(&tim,NULL);
				ps->q1EndTime=qonedeparttime=getseconds(tim)-getseconds(globaltim);
				printtime(tim);
				if(requiredtoken==1 || requiredtoken==0)
				{
				fprintf(stdout,"p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %ld token\n",ps->packetNum,(qonedeparttime-ps->q1ArrivalTime)/1000,requiredtoken);
				}
				else
				{
				fprintf(stdout,"p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %ld tokens\n",ps->packetNum,(qonedeparttime-ps->q1ArrivalTime)/1000,requiredtoken);	
				}
				//totalTimeOfAllPacketsInQone=totalTimeOfAllPacketsInQone+(qonedeparttime-ps->q1ArrivalTime);
				ps->q1EndTime=qonedeparttime;	
				//pthread_testcancel();	
				gettimeofday(&tim,NULL);				
				ps->q2ArrivalTime=(double)(getseconds(tim)-getseconds(globaltim));
				PacketsInQtwo++;	
				My402ListPrepend(&queue2,(void *)data);
				printtime(tim);		
				fprintf(stdout,"p%d enters Q2\n",ps->packetNum);
				if(++service==1)
				{
					pthread_cond_signal(&cv);
				}
				pthread_testcancel();
                        }
				
		}
		
	
                if(My402ListEmpty(&queue1) && (count == numberOfPackets))
		{
			break;
		}
		pthread_mutex_unlock(&m);
	}
	pthread_mutex_unlock(&m);
	pthread_mutex_lock(&m);
	pthread_cond_signal(&cv);
	pthread_mutex_unlock(&m);
	tokenexit=TRUE;
	pthread_exit(0);
}

void *serverthreadProcess(void *args)
{
	struct timeval tim;
	if(!files)
	{
		sleepservicetime=(1/mu)*(double)1000000;
		if((1/mu)>10)
			sleepservicetime=10000000;
	}
	while(1 && !shutdown)
	{		
		pthread_mutex_lock(&m);	
		while(service==0 && !shutdown && (!tokenexit) && (!arrivalexit))
		{
			pthread_cond_wait(&cv,&m);
			usleep(500);
			
		}
		if(!My402ListEmpty(&queue2) && !shutdown)
		{
		service--;
		My402ListElem *elem=My402ListLast(&queue2);
		void *data = (void *)elem->obj;
		My402ListUnlink(&queue2,elem);
		packet *p=(packet *)data;
		gettimeofday(&tim,NULL);
		p->q2EndTime=(double)getseconds(tim)-(double)getseconds(globaltim);
		if(shutdown)
		{	
			printtime(tim);
			fprintf(stdout,"p%d removed from Q2\n",p->packetNum);
			free(p);
		
		}
		else
		{
			printtime(tim);
			fprintf(stdout,"p%d leaves Q2, time in Q2 = %.3lfms\n",p->packetNum,((double)(p->q2EndTime)-(double)(p->q2ArrivalTime))/1000);
			//gettimeofday(&tim,NULL);
			//totalTimeOfAllPacketsInQtwo=totalTimeOfAllPacketsInQtwo+((double)(p->q2EndTime)-(double)(p->q2ArrivalTime));
			
		}		
		if(shutdown)
		{
			//printf("shutdown\n");
			break;
		}
		gettimeofday(&tim,NULL);
		p->serverArrivalTime=getseconds(tim)-(double)getseconds(globaltim);
		printtime(tim);
		fprintf(stdout,"p%d begins service at S, requesting %.3lfms of service\n",p->packetNum,(p->servicetimeofpacket)/1000);
		pthread_mutex_unlock(&m);				
		if(p->servicetimeofpacket>0)
		{
			usleep((useconds_t)p->servicetimeofpacket);
		}
		pthread_mutex_lock(&m);
		gettimeofday(&tim,NULL);
		p->serverEndTime=getseconds(tim)-(double)getseconds(globaltim);
		numofPacketsServed++;
		printtime(tim);
		fprintf(stdout,"p%d departs from S, service time = %.3lfms, time in system = %.3lfms\n",p->packetNum,((double)(p->serverEndTime)-(double)(p->serverArrivalTime))/1000,((double)(p->serverEndTime)-(double)(p->q1ArrivalTime))/1000);
		totalPacketsServiceTime=totalPacketsServiceTime+((double)(p->serverEndTime)-(double)(p->serverArrivalTime));
		totalTimeInSystem=totalTimeInSystem+((double)(p->serverEndTime)-(double)(p->q1ArrivalTime));
		squaretimespent=squaretimespent+((double)(p->serverEndTime)-(double)(p->q1ArrivalTime))*((double)(p->serverEndTime)-(double)(p->q1ArrivalTime));
		totalTimeOfAllPacketsInQone=totalTimeOfAllPacketsInQone+(p->q1EndTime-p->q1ArrivalTime);
		totalTimeOfAllPacketsInQtwo=totalTimeOfAllPacketsInQtwo+((double)(p->q2EndTime)-(double)(p->q2ArrivalTime));
		}
		else if(!My402ListEmpty(&queue1) && !shutdown)
		{
			pthread_mutex_unlock(&m);
			continue;
		}

		if((numofPacketsDropped+numofPacketsServed)==numberOfPackets)
		{
			if((tokenexit && arrivalexit))
			{ 
			gettimeofday(&tim,NULL);
			printtime(tim);
			fprintf(stdout,"emulation ends\n");
			printemulationend=TRUE;
			break;		
			}
			
		}
		pthread_mutex_unlock(&m);
		

	}
	
	pthread_mutex_unlock(&m);
	serverexit=1;
	
	pthread_exit(0);

}


void Cleanup()
{
	pthread_cond_signal(&cv);
	struct timeval tim;
	while(!My402ListEmpty(&queue1))
	{
		My402ListElem *elem=My402ListLast(&queue1);
		packet *p = (packet *)elem->obj;		
		My402ListUnlink(&queue1,elem);
		gettimeofday(&tim,NULL);
			printtime(tim);
		fprintf(stdout,"p%d removed from Q1\n",p->packetNum);
		free(p);
	}
	while(!My402ListEmpty(&queue2))
	{
		My402ListElem *elem=My402ListLast(&queue2);
		packet *p = (packet *)elem->obj;		
		My402ListUnlink(&queue2,elem);
		gettimeofday(&tim,NULL);
			printtime(tim);
		fprintf(stdout,"p%d removed from Q2\n",p->packetNum);
		free(p);
	}
	if(files)
	{
		if(fp)
		{
			fclose(fp);
			fp=NULL;		
		}
	}
	
}

void interrupt(int sig)
{
	
	pthread_mutex_unlock(&m);
	pthread_mutex_lock(&m);
	shutdown=1;
	pthread_cancel(arrival);
	arrivalexit=TRUE;
	pthread_cancel(token);
	tokenexit=TRUE;
	Cleanup();
	pthread_cond_signal(&cv);
	pthread_mutex_unlock(&m);
}	

void *sigintthreadprocess(void *args)
{
	
	act.sa_handler = interrupt;
	sigaction(SIGINT, &act, NULL);
	pthread_sigmask(SIG_UNBLOCK, &new, NULL);
	//printf("\n Press CTRL-C to deliver SIGINT\n");
	while(!(serverexit && arrivalexit && tokenexit))
	{
		sleep(2);
	}
	
	struct timeval tim;
	Cleanup();
	if(!printemulationend)
	{
		gettimeofday(&tim,NULL);
		printtime(tim);
		fprintf(stdout,"emulation ends\n");
	}
	pthread_exit(0);
}



int main(int argc, char *argv[])
{      
    	    int i,c=0;
	    int option_index = 0;
	    char value[10];

	    if(argc>1)
	    {
		if(argv[1][0]!='-')
		{
			fprintf(stderr,"Malformed command\n");
			Usage();
			exit(-1);
		}
 	    }
	    static struct option long_options[] =
        	{
          		{"lambda",required_argument, 0, 'l'},
          		{"mu",required_argument, 0, 'm'}
			
              	};
	    while ((c=getopt_long (argc, argv, "l:m:n:B:r:P:t",
                       long_options, &option_index))!=-1)
   	    {
      		switch (c)
        	{
        		case 'n': 
          			if(atoi(optarg)<=0 || (strchr(optarg,'.')!=NULL))
				{
					fprintf(stderr,"Malformed command - Invalid packet value 'n'\n");
					Usage();
					exit(-1);		
				}
				
				numberOfPackets=atoi(optarg);
				
          		break;
        		case 'B':
          			if(atoi(optarg)<=0 || (strchr(optarg,'.')!=NULL))
				{
					fprintf(stderr,"Malformed command - Invalid bucket size 'B'\n");
					Usage();
					exit(-1);		
				}
				B=atoi(optarg);
          		break;
        		case 'P':
          			if(atoi(optarg)<=0 || (strchr(optarg,'.')!=NULL))
				{
					fprintf(stderr,"Malformed command - Invalid token value 'P'\n");
					Usage();
					exit(-1);		
				}
				P=atoi(optarg);
          		break;

        		case 'm':
				memset(value,0,sizeof(value));
				if(argv[optind]==NULL)
				{
					fprintf(stderr,"Malformed command - service rate cannot be empty\n");
					Usage();
					exit(-1);
				}
          			strcpy(value,argv[optind]);	
				for(i=0;i<strlen(value);i++)
				{
					if((((value[i]-'0')<0) || ((value[i]-'0')>9)) && (value[i]!='.'))
					{
						fprintf(stderr,"Malformed command - Invalid service rate - 'mu'\n");
						Usage();
						exit(-1);
					}
				}
				mu=strtod(value,NULL);
          			
          		break;

        		case 'r':
          			
				for(i=0;i<strlen(optarg);i++)
				{
					if((((optarg[i]-'0')<0) || ((optarg[i]-'0')>9)) && (optarg[i]!='.'))
					{
						fprintf(stderr,"Malformed command - Invalid token rate - 'r'\n");
						Usage();
						exit(-1);
					}
				}
				r=strtod(optarg,NULL);
          		break;

        		case 'l':
				memset(value,0,sizeof(value));
				if(argv[optind]==NULL)
				{
					fprintf(stderr,"Malformed command - packet arrival rate cannot be empty\n");
					Usage();
					exit(-1);
				}
          			strcpy(value,argv[optind]);
				for(i=0;i<strlen(value);i++)
				{
					if((((value[i]-'0')<0) || ((value[i]-'0')>9)) && (value[i]!='.'))
					{
						fprintf(stderr,"Malformed command - Invalid packet arrival rate - 'lambda'\n");
						Usage();
						exit(-1);
					}
				}
				lambda=strtod(value,NULL);
          		break;
	
			case 't':
				if(argv[optind]==NULL)
				{
					fprintf(stderr,"Malformed command - filename cannot be empty\n");
					Usage();
					exit(-1);
				}
          			strcpy(fileName,argv[optind]);
				files=TRUE;
								
          		break;
			
       		        default:
      			fprintf(stderr,"Malformed command\n");
			Usage();
			break;
        	}
	    }
	    if(!files)
	    {
		if(mu==0 || r==0 || lambda==0 || B==0 || P==0 || numberOfPackets==0)
		{
			fprintf(stderr, "Malformed input - One of the passed values is not in proper format\n");
			exit(-1);
		}
	    }
	    else
	    {
		if(mu==0 ||  B==0 || (strlen(fileName)==0))
		{
			fprintf(stderr, "Malformed input - One of the passed values is not in proper format\n");
			exit(-1);
		}
	    }
	

        
	memset(&queue1,0,sizeof(My402List));
	memset(&queue2,0,sizeof(My402List));
	if(!My402ListInit(&queue1))
	{
		fprintf(stderr,"Queue 1 not initialized");
		exit(-1);
	}
	if(!My402ListInit(&queue2))
	{
		fprintf(stderr,"Queue 2 not initialized");
		exit(-1);
	}
       gettimeofday(&globaltim,NULL);  
       
	 sigemptyset(&new);
	 sigaddset(&new, SIGINT);
	 pthread_sigmask(SIG_BLOCK, &new, NULL);
         pthread_create(&arrival,NULL,arrivalthreadProcess, NULL);
        pthread_create(&token,NULL,tokenthreadProcess, NULL);
        pthread_create(&server,NULL,serverthreadProcess, NULL);
	pthread_create(&sigintthread,NULL,sigintthreadprocess, NULL);
        pthread_join(arrival,NULL);
	pthread_join(token,NULL);
	pthread_join(server,NULL);
	pthread_join(sigintthread,NULL);
	if(files)
	{
		if(fp)
		{
			fclose(fp);
		}
	}
	fprintf(stdout,"\nStatistics:\n\n");	
	if((count)==0)
	{
		fprintf(stdout,"\taverage packet inter-arrival time - N/A\n");
	}
	else
	{
		fprintf(stdout,"\taverage packet inter-arrival time = %lf\n",((totalInterArrivalTime)/(count))/1000000);
	}
	if(numofPacketsServed==0)
	{
		fprintf(stdout,"\taverage packet service time - N/A\n");
	}
	else
	{
		fprintf(stdout,"\taverage packet service time = %lf\n",((totalPacketsServiceTime)/(numofPacketsServed))/1000000);
	}
	fprintf(stdout,"\taverage number of packets in Q1 = %lf\n",(totalTimeOfAllPacketsInQone)/(emulationEndTime));
	fprintf(stdout,"\taverage number of packets in Q2 = %lf\n",(totalTimeOfAllPacketsInQtwo)/(emulationEndTime));
	fprintf(stdout,"\taverage number of packets in S = %lf\n",(totalPacketsServiceTime)/(emulationEndTime));
	if(numofPacketsServed==0)
	{
		fprintf(stdout,"\taverage time a packet spent in system - N/A\n");
	}
	else
	{
		fprintf(stdout,"\taverage time a packet spent in system = %lf\n",((totalTimeInSystem)/(numofPacketsServed))/1000000);
	}
	
	if(numofPacketsServed==0)
	{
		fprintf(stdout,"\tstandard deviation for time spent in system - N/A\n");
	}
	else
	{
	fprintf(stdout,"\tstandard deviation for time spent in system = %lf\n",(sqrt(((squaretimespent/numofPacketsServed)-((totalTimeInSystem/numofPacketsServed)*(totalTimeInSystem/numofPacketsServed)))))/1000000);
	}
	if(totaltokens==0)
	{
		fprintf(stdout,"\ttoken drop probability - N/A\n");
	}
	else
	{
		fprintf(stdout,"\ttoken drop probability = %lf\n",(double)(numOfDroppedTokens)/(totaltokens));
	}
	if((count)==0)
	{
		fprintf(stdout,"\tpacket drop probability - N/A\n");
	}
	else
	{
		fprintf(stdout,"\tpacket drop probability = %lf\n",(double)(numofPacketsDropped)/(count));
	}

return 0;
}


