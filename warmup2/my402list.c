#include<stdio.h>
#include<stdlib.h>
#include"my402list.h"

int My402ListInit(My402List *list)
{
    if(list==NULL)
       return FALSE;
    list->anchor.next=&list->anchor;
    list->anchor.prev=&list->anchor;
    list->anchor.obj=NULL;
    list->num_members=0;
    return TRUE;
}

int My402ListAppend(My402List *list, void *data)
{
      My402ListElem *temp,*last,*first=&list->anchor;
      temp = (My402ListElem *)malloc(sizeof(My402ListElem));
      if(temp==NULL)
        return FALSE;
      last=first->prev;
      temp->obj=(void*)data;
      temp->prev=last;
      temp->next=first;
      last->next=first->prev=temp;
      list->num_members++;
      return TRUE;
}

int My402ListPrepend(My402List *list, void *data)
{
      My402ListElem *temp,*second,*first=&list->anchor;
      temp = (My402ListElem *)malloc(sizeof(My402ListElem));
      if(temp==NULL)
        return FALSE;
      second=first->next;
      temp->obj=(void*)data;
      second->prev=first->next=temp;
      temp->prev=first;
      temp->next=second;
      list->num_members++;
      return TRUE;
}

int My402ListLength(My402List *list)
{
    return list->num_members;
}

int My402ListEmpty(My402List *list)
{
     if(list->anchor.next==&list->anchor && list->anchor.prev==&list->anchor)
       return TRUE;
    return FALSE;
}

void My402ListUnlink(My402List *list,My402ListElem *element)
{
     My402ListElem *temp=element->prev,*temp1=element->next;
     temp->next=element->next;
     temp1->prev=element->prev;
     free(element);     
     list->num_members--;
}

void My402ListUnlinkAll(My402List *list)
{
     My402ListElem *nextNode,*currNode,*freenode;
     if(list->anchor.next==&list->anchor && list->anchor.prev==&list->anchor)
       return;
     currNode = &list->anchor;
     nextNode = currNode->next;
     while(nextNode!=&list->anchor)
     {
       freenode=nextNode;
       currNode->next=freenode->next;
       freenode->next->prev=freenode->prev;
       nextNode = nextNode->next;
       free(freenode);
       list->num_members--;
     }
}

int My402ListInsertAfter(My402List* list,void *obj,My402ListElem *element)
{
     My402ListElem *temp;
    if(element==NULL) 
      return My402ListAppend(list,obj);
    else
    {
      temp=(My402ListElem *)malloc(sizeof(My402ListElem));
      temp->obj=obj;
      temp->next=element->next;
      temp->prev=element;
      element->next = temp->next->prev=temp;      
      list->num_members++;
      return TRUE;
    }
}

int My402ListInsertBefore(My402List* list,void *obj,My402ListElem *element)
{
    My402ListElem *temp;
    if(element==NULL)
      return My402ListPrepend(list,obj);
    else
    {
      temp=(My402ListElem *)malloc(sizeof(My402ListElem));
      temp->obj=obj;
      temp->prev=element->prev;
      temp->next=element;
      element->prev = temp->prev->next=temp;
      list->num_members++;
      return TRUE;
    }
}

My402ListElem *My402ListFirst(My402List *list)
{
     if(list->anchor.next==&list->anchor && list->anchor.prev==&list->anchor)
       return NULL;
     return list->anchor.next;     
}

My402ListElem *My402ListLast(My402List *list)
{
      if(list->anchor.next==&list->anchor && list->anchor.prev==&list->anchor)
       return NULL;
     return list->anchor.prev;
}

My402ListElem *My402ListNext(My402List *list,My402ListElem *nextnode)
{
     if(nextnode->next==&list->anchor)
       return NULL;
     return nextnode->next;
}

My402ListElem *My402ListPrev(My402List *list,My402ListElem *prevnode)
{
     if(prevnode->prev==&list->anchor)
       return NULL;
     return prevnode->prev;
}


My402ListElem *My402ListFind(My402List *list, void *obj)
{
     My402ListElem *node = list->anchor.next;
      if(list->anchor.next==&list->anchor && list->anchor.prev==&list->anchor)
        return NULL;
     while(node!=&list->anchor)
     {       
       if(node->obj==obj)
          return node;
       node=node->next;
     }
     return NULL;
}

